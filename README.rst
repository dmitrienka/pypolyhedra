===========
pypolyhedra
===========


.. image:: https://img.shields.io/pypi/v/pypolyhedra.svg
        :target: https://pypi.python.org/pypi/pypolyhedra

.. image:: https://img.shields.io/travis/dmitrienka/pypolyhedra.svg
        :target: https://travis-ci.com/dmitrienka/pypolyhedra

.. image:: https://readthedocs.org/projects/pypolyhedra/badge/?version=latest
        :target: https://pypolyhedra.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




Code for database analysis of coordination polihedra


* Free software: GNU General Public License v3
* Documentation: https://pypolyhedra.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

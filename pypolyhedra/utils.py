#!/usr/bin/env python3

def get_element(atom):
    import re
    rr = re.search('^[A-Z][a-z]?', atom).group(0)
    return(rr)

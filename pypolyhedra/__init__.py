"""Top-level package for pypolyhedra."""

__author__ = """Artem O. Dmitrienko"""
__email__ = 'dmitrienka@gmail.com'
__version__ = '0.1.0'

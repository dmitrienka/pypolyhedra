#!/usr/bin/env python3

import numpy as np
from . import utils

def bang(i_seq, structure, asu_mappings, raw_pair_table):
    scatterers = structure.scatterers()
    scatterer = scatterers[i_seq]
    unit_cell = structure.unit_cell()
    rt_mx_i_inv = asu_mappings.get_rt_mx(i_seq, 0).inverse()
    site_cart = unit_cell.orthogonalize(scatterer.site)
    center = scatterer.label
    polyhedra = []
    labels = []
    for j_seq, j_syms in raw_pair_table[i_seq].items():
        j_site_frac = scatterers[j_seq].site
        j_site_cart = unit_cell.orthogonalize(j_site_frac)
        for sym_group in j_syms:
            for j_sym in sym_group: # j_sym is an index into
                rt_mx = rt_mx_i_inv.multiply(asu_mappings.get_rt_mx(j_seq, j_sym))
                j_site_symm = rt_mx * j_site_frac
                j_site_symm_cart = unit_cell.orthogonalize(j_site_symm)
                distance = unit_cell.distance(scatterer.site, j_site_symm)
                labels.append(scatterers[j_seq].label)
                polyhedra.append([j_site_symm_cart[0] - site_cart[0],
                                  j_site_symm_cart[1] - site_cart[1],
                                  j_site_symm_cart[2] - site_cart[2],])
    record = {'metal_name':center, 'metal':utils.get_element(center),
              'polyhedra':np.array(polyhedra), 'atom_names':labels,
              'atoms':np.array([utils.get_element(a) for a in labels])}
    return(record)


def prepare_mappings(structure, cutoff):
    from cctbx import crystal
    asu_mappings = structure.asu_mappings(buffer_thickness = cutoff)
    pair_asu_table = crystal.pair_asu_table(asu_mappings = asu_mappings)
    pair_asu_table.add_all_pairs(distance_cutoff = cutoff)
    raw_pair_table = pair_asu_table.table()
    return(asu_mappings, raw_pair_table)

def type_bangs(structure, atom_types, cutoff) :
  asu_mappings, raw_pair_table = prepare_mappings(structure, cutoff)
  result = []
  for i_seq, scatterer in enumerate(structure.scatterers()):
      label = scatterer.element_symbol().strip()
      if label in atom_types:
          result.append(bang(i_seq, structure, asu_mappings, raw_pair_table))
  return(result)


## example
## rrr= [ dict(b, ID = f"{k}_{i}")  for k in dd.keys() for i,b in enumerate(type_bangs(dd[k], "Cu", 2.5 ))]

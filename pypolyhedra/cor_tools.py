#!/usr/bin/env python3

def read_cor(f, size):
    def _read_cor_chunk2(f, size):
        import re
        import numpy as np
        header = f.readline()
        if not header:
            return(None)
        header_groups = re.search(r'([A-Z]{6}[0-9]?[0-9]?)[ ]*\*\*FRAG\*\*[ ]+([0-9]+)', header)
        ref = header_groups.group(1)
        frag = int(header_groups.group(2))
        chunk = np.array([  [v for v in line.split()[0:-1]]  for line in islice(f, size)]).T
        coords = chunk[1:4,].astype(float)
        atoms = chunk[0]
        result = {'refcode':ref, 'fragment':frag, 'atoms':atoms, 'coordinates':coords }
        return(result)
    result = []
    while True:
        chunk = _read_cor_chunk2(f, size)
        if chunk is None:
            break
        result.append(chunk)
    return result

def make_polyhedra(cor, metal_position = 0):
    co = cor['coordinates']
    nrow  = co.shape[1]
    poly = co - np.tile(co[:,metal_position], (nrow,1)).T
    poly = np.delete(poly, metal_position, axis=1)
    return ({'ID':cor['refcode'] + "_" + str(cor['fragment']),
             'polyhedra':poly.T,
             'metal':re.search('^[A-Z][a-z]?', cor['atoms'][metal_position]).group(0)})

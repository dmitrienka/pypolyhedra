=======
Credits
=======

Development Lead
----------------

* Artem O. Dmitrienko <dmitrienka@gmail.com>

Contributors
------------

None yet. Why not be the first?

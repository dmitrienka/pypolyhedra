#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = ['Click>=7.0', ]

test_requirements = [ ]

setup(
    author="Artem O. Dmitrienko",
    author_email='dmitrienka@gmail.com',
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="Code for database analysis of coordination polihedra",
    entry_points={
        'console_scripts': [
            'pypolyhedra=pypolyhedra.cli:main',
        ],
    },
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='pypolyhedra',
    name='pypolyhedra',
    packages=find_packages(include=['pypolyhedra', 'pypolyhedra.*']),
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/dmitrienka/pypolyhedra',
    version='0.1.0',
    zip_safe=False,
)
